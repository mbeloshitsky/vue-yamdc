import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import BaseButton from '../src/BaseButton'
import Theme from './Theme'

export default {
  title: 'Button',
  component: BaseButton,
}

export const Text = () => ({
  components: { Theme, BaseButton },
  template: '<theme><base-button @click="action">Hello Button</base-button></theme>',
  methods: { action: action('clicked') },
})

export const Emoji = () => ({
  components: { Theme, BaseButton },
  template: '<theme><base-button @click="action">😀 😎 👍 💯</base-button></theme>',
  methods: { action: action('clicked') },
})