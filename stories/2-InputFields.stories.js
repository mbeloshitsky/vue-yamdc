import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import YaTextField from '../src/TextField'
import YaNumberField from '../src/NumberField'
import YaPasswordField from '../src/PasswordField'
import Theme from './Theme'

export default {
  title: 'Input Fields'
}

export const TextField = () => ({
  components: { Theme, YaTextField },
  data () {
    return {
      sampleText: 'Sample text'
    }
  },
  template: '<theme><ya-text-field label="Title" v-model="sampleText" @input="action" /></theme>',
  methods: { action: action('input') },
})

export const NumberField = () => ({
  components: { Theme, YaNumberField },
  template: '<theme><ya-number-field label="Title" @input="action" /></theme>',
  methods: { action: action('input') },
})

export const PasswordField = () => ({
  components: { Theme, YaPasswordField },
  data () {
    return {
      password: ''
    }
  },
  template: '<theme><ya-password-field label="Title" v-model="password" @input="action" /></theme>',
  methods: { action: action('input') },
})