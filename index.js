import BaseButton from './src/BaseButton'

import BaseField from './src/BaseField'
import TextField from './src/TextField'
import NumberField from './src/NumberField'
import PasswordField from './src/PasswordField'

import MultiselectField from './src/MultiselectField'
import MulticheckField from './src/MulticheckField'

const components = {
    BaseButton,
    BaseField,
    TextField,
    NumberField,
    PasswordField,
    MulticheckField,
    MultiselectField
}

export default {
    async install (Vue) {
        for(let name in components) {
            Vue.component('Ya' + name, components[name])
        }
    }
}