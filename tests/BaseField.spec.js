import { shallowMount } from '@vue/test-utils'
import BaseField from '../src/BaseField'

describe('BaseField', () => {
    it('renders correctly', () => {
        expect(shallowMount(BaseField).element).toMatchSnapshot()
    })
})