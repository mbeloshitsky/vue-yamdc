import { shallowMount } from '@vue/test-utils'
import MulticheckField from '../src/MulticheckField'

describe('MulticheckField', () => {
    it('renders correctly', () => {
        expect(shallowMount(MulticheckField).element).toMatchSnapshot()
    })
})