import { shallowMount } from '@vue/test-utils'
import PasswordField from '../src/PasswordField'

describe('PasswordField', () => {
    it('renders correctly', () => {
        expect(shallowMount(PasswordField).element).toMatchSnapshot()
    })
})