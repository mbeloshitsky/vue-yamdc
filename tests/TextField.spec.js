import { shallowMount } from '@vue/test-utils'
import TextField from '../src/TextField'

describe('TextField', () => {
    it('renders correctly', () => {
        expect(shallowMount(TextField).element).toMatchSnapshot()
    })
})