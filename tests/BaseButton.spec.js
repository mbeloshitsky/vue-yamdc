import { shallowMount } from '@vue/test-utils'
import BaseButton from '../src/BaseButton'

describe('BaseButton', () => {
    it('renders correctly', () => {
        expect(shallowMount(BaseButton).element).toMatchSnapshot()
        expect(shallowMount(BaseButton, { slots: { default: [ 'title' ] } }).element).toMatchSnapshot()
        expect(shallowMount(BaseButton, { propsData: { disabled: true }}).element).toMatchSnapshot()
    })
})