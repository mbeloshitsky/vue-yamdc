import { shallowMount } from '@vue/test-utils'
import NumberField from '../src/NumberField'

describe('NumberField', () => {
    it('renders correctly', () => {
        expect(shallowMount(NumberField).element).toMatchSnapshot()
    })
})